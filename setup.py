from setuptools import find_packages
from setuptools import setup


setup(
    name='pre_commit_hooks',
    description='Spellcheck for pre-commit.',
    url='https://gitlab.com/tomleo/git-pre-commit-hooks',
    version='0.0.1',

    author='Thomas Leo',
    author_email='tom@tomleo.com',

    classifiers=[
        'License :: OSI Approved :: GNU General Public License (GPL)',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: Implementation :: CPython',
        'Programming Language :: Python :: Implementation :: PyPy',
    ],

    packages=find_packages(exclude=('tests*', 'testing*')),
    install_requires=[
        # quickfix to prevent pep8 conflicts
        'scspell3k'
    ],
    entry_points={
        'console_scripts': [
            'spell-check-python = pre_commit_hooks.spell_check_python:check_python_spelling',
        ],
    },
)

